# GitLab CI:  Deployment to Apache Tomcat without Docker

---

**Requirements**
- Local Specific Gitlab CI Runner installed 
- Java 8+, Apache Maven 3, and Apache Tomcat 8+ installed 

## Configuration a local Specific GitLab CI Runner on Windows
Follow the instructions from the [previous step](https://gitlab.com/mromdhani/04-gitlabci-specific-runner) to install and start a specific GitLabCI Runner (In this exmaple the Runner has a `shell` executor and is tagged as `mymavenrunner`)
![The runner capture](images/therunner.jpg)

## Java Web Application to deploy
The Java Web Application follows the maven conventions. Its code is available in the repository of this project. It is composed of a simple greeting web page (*index.jsp*).
The figure below shows the structure of the web appliaction.
![The webapp capture](images/weapp.jpg)

## The .gitlab-ci.yml

The pipeline is composed of two stages. 
1. To make the build project a warfile we use the following command. A war file under target will be generated:
    ```shell 
    mvn clean package
    ```
2. Then we copy the war from *target/* to the *webapps* folder of Apache Tomcat.
   
**Verifications**
- Check that the pipeline succeeds.
  ![The build result caputure](images/buildresult.jpg)
- Access the deployed app using the URL `http://localhost:8080/greetings/`